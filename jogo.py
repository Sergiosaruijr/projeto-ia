import sys
import pygame
import main
import cProfile

def play_geral():
    main.iniciar_jogo()
    pygame.quit()
    sys.exit()

if __name__ == '__main__':
    play_geral()
