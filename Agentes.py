import pygame
import regras_jogo as rj

pygame.init()

class inimigo(object): 
    def __init__(self, x, y, width, height, end):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.vel = 3
        self.end = end
        self.path = [self.x, self.end]
        self.andarContador = 0
        self.hitbox = (self.x - 5, self.y - 1, 33, 33)
        self.vida = 1
        self.visible = True
        
    def desenho(self,tela):
        #self.move()
        rj.tela.blit(rj.caco,(self.x, self.y))
        #pygame.draw.rect(tela,(0,255,0), (self.hitbox[0],self.hitbox[1] -20, 50, 10)
        self.hitbox = (self.x - 5, self.y - 1, 33, 33)
        #pygame.draw.rect(tela,(255,0,0), self.hitbox,2)
        
        
    def colisao(self):
        if self.vida > 0:
            self.vida -= 1
        else:
            self.visible = False
            print("hit")
            

# Implemente seu jogador humano nessa classe, sobrescrevendo os métodos
# abstratos de Agente. Em construir_agente, retorne uma instância dessa classe.
class player(object):
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.vel = 5
        self.left = False
        self.right = False
        self.andarContador = 0
        self.hitbox = (self.x - 2, self.y -5, 30, 40)
        self.vida = 30
    
    def desenho(self,tela):  # 50 - (5 * (10 - self.vida)), 10))
        rj.tela.blit(rj.char,(self.x, self.y))
        self.hitbox = (self.x - 2, self.y -5, 30, 40)
        pygame.draw.rect(rj.tela,(255,0,0), (self.hitbox[0] ,self.hitbox[1] +40,  30 , 5)) #barra de vida verde
        pygame.draw.rect(rj.tela,(0,255,0), (self.hitbox[0],self.hitbox[1] + 40, 30, 5))


    def colisao(self):
        if self.vida > 0:
            self.vida -= 10

class projetil(object):
    def __init__(self,x,y,radius,color):
        self.x = x
        self.y = y
        self.radius = radius
        self.color = color
        self.vel = 1 
        self.estado = "preparado"
        
    def desenho(self,tela):
        pygame.draw.circle(rj.tela,self.color,(self.x, self.y),self.radius)


