import pygame
import main


##estados##
run = True
gameOver = False
gamePlay = False
menu = False
menuTipoJogador = True
creditos = False
gameWin = False
dificuldade = 0

pygame.init()


##TELA##
tela = pygame.display.set_mode((500, 500))
pygame.display.set_caption("Space Invaders")
clock = pygame.time.Clock()
score = 0

# carregando fonte
font = pygame.font.SysFont(None, 25)
fonte = pygame.font.SysFont('comicsans', 30, True)
texto = fonte.render('Score: ' + str(score), 1, (0, 100, 200))

# definindo cores
preto = (0, 0, 0)
branco = (255, 255, 255)
azul = (0, 0, 255)
verde = (0, 255, 0)
vermelho = (255, 0, 0)

##IMAGENS##
fundo = pygame.image.load('fundo2.png')
fundoMenu = pygame.image.load('TesteFundo3.PNG')
fundoGameOver = pygame.image.load('TesteFundoGameOver.PNG')
fundoGameWin = pygame.image.load('FundoWin.png')
fundoGameWin2 = pygame.image.load('telaGameWin2.png')
fundoCredito = pygame.image.load('FundoC.png')
fundoTipoJogador = pygame.image.load('menuTipoJogador.png')
char = pygame.image.load('nave1.png')
laser = pygame.image.load('laser2.png')
caco = pygame.image.load('inimigo.png')  

def construir_jogo():
    tela.blit(fundo,(0, 0))
    texto = fonte.render('Score: ' + str(score), 1, (0, 100, 200))
    tela.blit(texto, (375, 10))
    main.nave.desenho(tela)
    for main.bullet in main.bullets:
        main.bullet.desenho(tela)
    for main.inimigo in main.invasores:
        main.inimigo.desenho(tela)
    pygame.display.update()

def gameLoop():
    run = True
    gameOver = False
    gamePlay = False
    tela.blit(fundo, (0, 0))
    texto = fonte.render('Score: ' + str(score), 1, (0, 100, 200))
    tela.blit(texto, (375, 10))
    main.nave.desenho(tela)
    for main.bullet in main.bullets:
        main.bullet.desenho(tela)
    for main.inimigo in main.invasores:
        main.inimigo.desenho(tela)
    pygame.display.update()

def textoTitulo(mensagem,cor):
    # definindo o texto (font.render - desenha texto sobre superficie)
    msg = font.render(mensagem, True, cor)

    # copiando o texto para a superfície
    tela.blit(msg, [190, 50])
    # atualizando a tela
    pygame.display.flip()


def textoInicio(mensagem,cor):
    # definindo o texto (font.render - desenha texto sobre superficie)
    msg = font.render(mensagem, True, cor)

    # copiando o texto para a superfície
    tela.blit(msg, [60, 200])
    # atualizando a tela
    pygame.display.flip()

def textoRodaPe(mensagem,cor):
    # definindo o texto (font.render - desenha texto sobre superficie)
    msg = font.render(mensagem, True, cor)

    # copiando o texto para a superfície
    tela.blit(msg, [100, 300])
    # atualizando a tela
    pygame.display.flip()

def textoVitoria(mensagem,cor):
    # definindo o texto (font.render - desenha texto sobre superficie)
    msg = font.render(mensagem, True, cor)

    # copiando o texto para a superfície
    tela.blit(msg, [40, 190])
    # atualizando a tela
    pygame.display.flip()


def textoFim(mensagem,cor):
    # definindo o texto (font.render - desenha texto sobre superficie)
    msg = font.render(mensagem, True, cor)

    # copiando o texto para a superfície
    tela.blit(msg, [190, 190])
    # atualizando a tela
    pygame.display.flip()



# class RegrasJogo(object):
#
# def isFim():
#     """ Boolean indicando fim de jogo em True.
#     """
#     run = False
#     gameOver = False
#     return run, gameOver
#
# def atualizarEstado():
#     if run == True:
#         gameOver = False
#     elif gameOver == True:
#         run = False
#     return run, gameOver
#

