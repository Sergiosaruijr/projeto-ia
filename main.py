
import pygame
import regras_jogo as rj
import Agentes as ag


try:
    pygame.init()
except:
    print("O modulo pygame não foi inicializado com sucesso")

##LOOP##
nave = ag.player(240, 410, 25, 24)
bullets = []
invasorrr = 0 #inimigo(10 + i * 40, 10, 24, 24, 450)
invasores = []
# n_invasores = 6

def reinciar_jogo():
    global run, gameOver, score, menu
    rj.run = True
    rj.menu = False
    rj.gameOver = False
    rj.gameWin = False
    rj.menuTipoJogador = True
    rj.score = 0
    rj.dificuldade = 0
    iniciar_jogo()


def iniciar_jogo():
    pygame.display.update()
    rj.score = 0
    rj.run = True
    rj.menu = False
    rj.gameOver = False
    rj.gameWin = False
    rj.tela.fill((0, 0, 0))
    if rj.dificuldade == 0:
        for i in range(2):
            for coluna in range(2):
                invasores.append(ag.inimigo(10 + i * 50, 10 + coluna * 30, 24, 24, 400))
    elif rj.dificuldade == 1:
        for i in range(4):
            for coluna in range(4):
                invasores.append(ag.inimigo(10 + i * 50, 10 + coluna * 30, 24, 24, 400))
    elif rj.dificuldade == 2:
        for i in range(6):
            for coluna in range(6):
                invasores.append(ag.inimigo(10 + i * 50, 10 + coluna * 30, 24, 24, 400))
    shootLoop = 0
    pygame.display.update()

    while rj.run:
        while rj.menuTipoJogador:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    key = event.key
                    if key == pygame.K_1:
                        rj.menu = True
                        rj.menuTipoJogador = False
                    if key == pygame.K_2:
                        rj.menu = True
                        rj.menuTipoJogador = False
            rj.clock.tick(60)
            rj.tela.blit(rj.fundoTipoJogador, (0, 0))
            pygame.display.update()
        while rj.menu:
            while rj.creditos:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        pygame.quit()
                        quit()
                    elif event.type == pygame.KEYDOWN:
                        key = event.key
                        if key == pygame.K_c:
                            rj.creditos = False

                rj.clock.tick(60)
                rj.tela.blit(rj.fundoCredito, (0, 0))
                pygame.display.update()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    key = event.key
                    if key == pygame.K_p:
                        rj.menu = False
                    if key == pygame.K_c:
                        rj.creditos = True

            rj.clock.tick(60)
            rj.tela.blit(rj.fundoMenu, (0, 0))
            pygame.display.update()

        rj.clock.tick(60)
        keys = pygame.key.get_pressed()
        pygame.display.update()




        while rj.gameOver == True:
            pygame.display.update()
            rj.tela.blit(rj.fundoGameOver, (0, 0))
            rj.textoFim('Score total: ' + str(rj.score), rj.azul)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    rj.run = False
                    rj.gameOver = False
                if event.type == pygame.KEYDOWN:
                    key = event.key
                    if key == pygame.K_r:
                        invasores.clear()
                        # reinciar_jogo()
                        # NAO ESQUECE DE COMENTAR O DE BAIXO E DESCOMENTAR O DE CIMA
                        if rj.dificuldade < 2:
                            rj.dificuldade += 1
                            iniciar_jogo()
                        elif rj.dificuldade == 2:
                            reinciar_jogo()
                    if key == pygame.K_s:
                        rj.run = False
                        rj.gameOver = False

        while rj.gameWin == True:
            pygame.display.update()
            rj.tela.blit(rj.fundoGameWin2, (0, 0))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    rj.run = False
                    rj.gameWin = False
                if event.type == pygame.KEYDOWN:
                    key = event.key
                    if key == pygame.K_n:
                        # rj.dificuldade += 1
                        # reinciar_jogo()
                        invasores.clear()
                        if rj.dificuldade < 2:
                            rj.dificuldade += 1
                            iniciar_jogo()
                        elif rj.dificuldade == 2:
                            reinciar_jogo()
                    if key == pygame.K_s:
                        rj.run = False
                        rj.gameWin = False




        pygame.display.update()

        if shootLoop > 0:
            shootLoop += 1
        if shootLoop > 3:
            shootLoop = 0

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                rj.run = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_p:
                    rj.menu = True

        if not invasores:
            rj.gameWin = True


        for inimigo in invasores:
            inimigo.x += inimigo.vel
            if inimigo.x > 450:
                inimigo.y += 40
                inimigo.vel *= -1
            if inimigo.x < 10:
                inimigo.y += 40
                inimigo.vel *= -1


            for bullet in bullets:
                if bullet.y  < inimigo.hitbox[1] + inimigo.hitbox[3] and bullet.y > inimigo.hitbox[1]:
                    if bullet.x > inimigo.hitbox[0] and bullet.x < inimigo.hitbox[0] + inimigo.hitbox[2]:
                        inimigo.colisao()
                        rj.score += 10
                        bullets.pop(bullets.index(bullet))
                        invasores.pop(invasores.index(inimigo))


                if bullet.y < 500 and bullet.y > 0:
                    bullet.y -= bullet.vel
                else:
                    bullets.pop(bullets.index(bullet))


            if nave.y  < inimigo.hitbox[1] + inimigo.hitbox[3] and nave.y > inimigo.hitbox[1]:
                if nave.x > inimigo.hitbox[0] and nave.x < inimigo.hitbox[0] + inimigo.hitbox[2]:
                    rj.gameOver = True
                    pygame.display.update()





        ##TECLADO##
        if keys[pygame.K_SPACE] and shootLoop == 0:
            if len(bullets) < 1:
                bullets.append(ag.projetil(round(nave.x + nave.width//2), round(nave.y + nave.height//2), 6, (255,0,0)))
            shootLoop = 1
        if keys[pygame.K_LEFT] and nave.x > nave.vel:
            nave.x -= nave.vel
            nave.right = False
            nave.left = True
        elif keys[pygame.K_RIGHT] and nave.x < 500 - nave.width - nave.vel:
            nave.x += nave.vel
            nave.right = True
            nave.left = False

        rj.tela.fill((0, 0, 0))
        rj.construir_jogo()
        pygame.display.update()

